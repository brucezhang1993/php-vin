<?php
namespace brucezhang1993\phpvin;
use Yangqi\Htmldom\Htmldom;

class Vin
{

    public static function decode($vin)
	{
		if (strlen($vin) != 17) {
			return array('errcode'=>'1', 'errmsg'=>'VIN格式不正确');
		}

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "http://www.fenco.cn/Index/search.html?word={$vin}");
		curl_setopt($curl, CURLOPT_POST, 0);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0'
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		$output = curl_exec($curl);

		$html = new Htmldom($output);
		$table = $html->find('table', 0);

		if (!$table) {
			return array('errcode'=>'2', 'errmsg'=>'查询不到结果');
		}

		$row = $table;
		
		$keys = array('vendor', 'brand', 'series', 'name', 'year', 'type', 'engine', 'gearbox', 'frameno', 'driving', 'produce_year', 'stop_year');

		foreach ($row->find('tr') as $k => $v) {
			$data[$keys[$k]] = $v->find('td', 0)->plaintext;
		}

		if (count($data) < 12) {
			return array('errcode'=>0, 'errmsg'=>'查询成功，数据不完整', 'data'=>$data);
		} else {
			return array('errcode'=>0, 'errmsg'=>'查询成功', 'data'=>$data);
		}

	}
}
