## PHP VIN Decode Library

---

- [GitLab](https://gitlab.com/brucezhang1993/php-vin)
- [Packagist](https://packagist.org/packages/brucezhang1993/php-vin)

---

### Require

- **With Composer(Recommand)**:

    `composer require brucezhang1993/php-vin`

- Old style:

    [Download Source Here](https://gitlab.com/brucezhang1993/php-vin/raw/master/brucezhang1993/phpvin/Vin.php)
    
    You're on your own from now.

### Usage

```php
# Composer autoload
require_once './vendor/autoload.php';

$handler = new brucezhang1993\phpvin\Vin;
$data = $handler->decode('17 digits VIN');
var_dump($data);
```

### Output Example

```php
$output = [
    'errocde' => '0',
    'errmsg' => '查询成功',
    'data' => [
        'vendor' => 'VENDOR',
        'brand' => 'BRAND',
        'series' => 'SERIES',
        'name' => 'CAR_NAME_FOR_SALE',
        'year' => 'YEAR_OF_CAR_TYPE',
        'type' => 'CAR_TYPE',
        'engine' => 'ENGINE_TYPE',
        'gearbox' => 'GEARBOX_TYPE',
        'frameno' => 'CHASSIS_NO',
        'driving' => 'DRIVING_METHOD',
        'produce_year' => 'YEAR_OF_START_PRODUCING',
        'stop_year' => 'YEAR_OF_STOP_PRODUCING'
    ]
];
```

### License

[MIT](https://gitlab.com/brucezhang1993/php-vin/blob/master/LICENSE)

The MIT License (MIT)

Copyright (c) 2018 Bruce Zhang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
